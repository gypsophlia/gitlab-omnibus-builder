# Install Git for use by omnibus
#
# Git will be built from source and installed in /usr/local/bin.

git_version = node['gitlab-omnibus-builder']['git_version']
git_tarball = node['gitlab-omnibus-builder']['git_tarball']
git_url = "https://www.kernel.org/pub/software/scm/git/#{git_tarball}"
git_tarball_sha256 = node['gitlab-omnibus-builder']['git_tarball_sha256']
local_git_tarball = "/var/cache/#{git_tarball}"

case node['platform_family']
when 'debian'
  packages = %w{
    gcc libcurl4-gnutls-dev libexpat1-dev gettext
    libz-dev libssl-dev make
  }
when 'rhel'
  packages = %w{
    gcc curl-devel expat-devel gettext-devel openssl-devel
    perl-devel zlib-devel make
  }
end

packages.each do |pkg|
  package pkg
end

remote_file local_git_tarball do
  source git_url
  checksum git_tarball_sha256
end

bash "install git #{git_version}" do
  code <<-EOS
set -e # Exit on failure
cd /var/cache
tar xzf #{local_git_tarball}
cd /var/cache/git-#{git_version}
make prefix=/usr/local install
EOS
  not_if "/usr/local/bin/git --version | grep '#{git_version}'"
end
