go_version = node['gitlab-omnibus-builder']['go_version']
go_tarball = node['gitlab-omnibus-builder']['go_tarball']
go_url = "https://storage.googleapis.com/golang/#{go_tarball}"
go_tarball_sha256 = node['gitlab-omnibus-builder']['go_tarball_sha256']
local_go_tarball = "/var/cache/#{go_tarball}"

remote_file local_go_tarball do
  source go_url
  checksum go_tarball_sha256
end

bash "install go #{go_version}" do
  code <<-EOS
set -e # Exit on failure
cd /usr/local
rm -rf go # Do not unpack new go on top of an old one
tar xzf #{local_go_tarball}
ln -sf /usr/local/go/bin/go \
  /usr/local/go/bin/gofmt \
  /usr/local/go/bin/godoc \
  /usr/local/bin/
EOS
  not_if "/usr/local/bin/go version | grep 'go#{go_version}'"
end
