FROM balenalib/rpi-raspbian:stretch

# Install required packages
RUN apt-get update -q \
  && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
      apt-utils \
      build-essential \
      autoconf \
      ca-certificates \
      gettext \
      locales \
      zlib1g-dev \
      libssl-dev \
      libcurl4-openssl-dev \
      curl \
      libexpat1-dev \
      libz-dev

RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN update-ca-certificates -f

ENV GIT_VERSION 2.22.0
RUN curl -fsSL "https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz" \
	| tar -xzC /tmp \
  && cd /tmp/git-${GIT_VERSION} \
  && ./configure \
  && make all \
  && make install
